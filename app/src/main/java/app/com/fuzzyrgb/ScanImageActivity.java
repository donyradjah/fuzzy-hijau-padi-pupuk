package app.com.fuzzyrgb;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.sdsmdg.tastytoast.TastyToast;

import org.eazegraph.lib.charts.ValueLineChart;
import org.eazegraph.lib.models.ValueLinePoint;
import org.eazegraph.lib.models.ValueLineSeries;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class ScanImageActivity extends AppCompatActivity {
    Button take, scan;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_GALLERY = 2;
    ProgressDialog loading;
    int greenDepthDuaTiga, greenDepthTigaEmpat, greenDepthEmpatLima;
    ImageView mImageView, mImageViewBinary;
    String ImageDecode, urlgambar, token, status, name, reason, uploadImage = "";
    Bitmap bitmap;
    TextView tv_1, tv_2;
    ValueLineChart mCubicValueLineChart, mCubicValueLineChart2, mCubicValueLineChart3, mCubicValueLineChart4;
    ValueLineSeries series, series2, series3, series4;
    private static final int MAX_INTENSITY = (int) Math.pow(2, 8) - 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_image);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        take = (Button) findViewById(R.id.take);
        tv_1 = (TextView) findViewById(R.id.tv_1);
        tv_2 = (TextView) findViewById(R.id.tv_2);
        mImageView = (ImageView) findViewById(R.id.img);
        mImageViewBinary = (ImageView) findViewById(R.id.img2);
        take.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImageOption();
            }
        });
        mCubicValueLineChart = (ValueLineChart) findViewById(R.id.cubiclinechart);
        mCubicValueLineChart2 = (ValueLineChart) findViewById(R.id.cubiclinechart2);
        mCubicValueLineChart3 = (ValueLineChart) findViewById(R.id.cubiclinechart3);
        mCubicValueLineChart4 = (ValueLineChart) findViewById(R.id.cubiclinechart4);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // action with ID action_refresh was selected
            case R.id.action_refresh:
                finish();
                break;
            // action with ID action_settings was selected

            default:
                break;
        }

        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        loading.dismiss();
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");

            mImageView.setImageBitmap(imageBitmap);
            new scan().execute(imageBitmap);
            BitmapDrawable drawable = (BitmapDrawable) mImageView.getDrawable();
            bitmap = drawable.getBitmap();
        }

    }
    public String BitMapToString(Bitmap bitmap) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
        byte[] b = baos.toByteArray();
        String temp = Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
    private void selectImageOption() {
        final CharSequence[] items = {"Ambil Foto",
                "Batal"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ScanImageActivity.this);
        builder.setTitle("Add Photo!!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Ambil Foto")) {
                    dispatchTakePictureIntent();

                } else if (items[item].equals("Batal")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }



    public static int mode(int[] array) {
        HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
        int max = 1, temp = 0;
        for (int i = 0; i < array.length; i++) {
            if (hm.get(array[i]) != null) {
                int count = hm.get(array[i]);
                count = count + 1;
                hm.put(array[i], count);
                if (count > max) {
                    max = count;
                    temp = array[i];
                }
            } else {
                hm.put(array[i], 1);
            }
        }
        return temp;
    }

    public Bitmap resizeBitmap(Bitmap image) {
        image = Bitmap.createScaledBitmap(image, 20, 20, true);
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            loading = ProgressDialog.show(ScanImageActivity.this, "Mengambil gambar", "Mohon tunggu...", true, false);
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    class scan extends AsyncTask<Bitmap, Void, int[]> {
        ProgressDialog dialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(ScanImageActivity.this);
            dialog.setMessage("Mohon tunggu... ");
            dialog.show();
            dialog.setCancelable(false);
        }

        @Override
        protected int[] doInBackground(Bitmap... bitmaps) {
            series = new ValueLineSeries();
            series.setColor(0xFF000000);
            series2 = new ValueLineSeries();
            series2.setColor(0xFFFF0000);
            series3 = new ValueLineSeries();
            series3.setColor(0xFF7CFC00);
            series4 = new ValueLineSeries();
            series4.setColor(0xFF0000FF);
            int width, height;
            Bitmap bmpOriginal = resizeBitmap(bitmaps[0]);
            height = bmpOriginal.getHeight();
            width = bmpOriginal.getWidth();
            int[] red = new int[width * height];
            int[] green = new int[width * height];
            int[] blue = new int[width * height];
            int[] srgb = new int[width * height];
            int[] hasil = new int[10];
            List<Integer> srgbl = new ArrayList<Integer>();
            List<Integer> redl = new ArrayList<Integer>();
            List<Integer> bluel = new ArrayList<Integer>();
            List<Integer> greenl = new ArrayList<Integer>();

            double r1 = 0, r2 = 0, g1 = 0, g2 = 0, b1 = 0, b2 = 0;
            int statr1 = 0, statr2 = 0, statg1 = 0, statg2 = 0, statb1 = 0, statb2 = 0, z1 = 0, z2 = 0;
            Log.wtf("jml pixel  ", "" + (width * height));
            int j = 0;
            for (int x = 0; x < width; ++x) {
                for (int y = 0; y < height; ++y) {

                    //get one pixel color
                    int pixel = bmpOriginal.getPixel(x, y);
                    red[j] = (pixel >> 16) & 0xff;
                    green[j] = (pixel >> 8) & 0xff;
                    blue[j] = (pixel) & 0xff;
                    srgb[j] = red[j] + green[j] + blue[j];
//                    Log.wtf("pixel : " + j, "Hasil modus S-RGB : " + srgb[j] + " ,modus R : " + red[j] + " modus G : " + green[j] + " modus B : " + blue[j]);

                    if (!srgbl.contains(srgb[j])) {
                        srgbl.add(srgb[j]);
                    }
                    if (!redl.contains(red[j])) {
                        redl.add(red[j]);
                    }
                    if (!bluel.contains(green[j])) {
                        bluel.add(blue[j]);
                    }
                    if (!greenl.contains(green[j])) {
                        greenl.add(green[j]);
                    }
                    j++;

                }
            }
            int[] srgbunique = convertIntegers(srgbl);
            int[] redunique = convertIntegers(redl);
            int[] blueunique = convertIntegers(bluel);
            int[] greenunique = convertIntegers(greenl);
            for (int s : srgbunique) {
                Log.wtf("" + s, "" + (float) getMaxValue(s, srgb));
                series.addPoint(new ValueLinePoint("" + s, (float) getMaxValue(s, srgb)));
            }
            for (int s : redunique) {
                Log.wtf("" + s, "" + (float) getMaxValue(s, srgb));
                series2.addPoint(new ValueLinePoint("" + s, (float) getMaxValue(s, red)));
            }
            for (int s : blueunique) {
                Log.wtf("" + s, "" + (float) getMaxValue(s, srgb));
                series3.addPoint(new ValueLinePoint("" + s, (float) getMaxValue(s, green)));
            }
            for (int s : greenunique) {
                Log.wtf("" + s, "" + (float) getMaxValue(s, srgb));
                series4.addPoint(new ValueLinePoint("" + s, (float) getMaxValue(s, blue)));
            }
            hasil[0] = mode(srgb);
            hasil[1] = mode(red);
            hasil[2] = mode(green);
            hasil[3] = mode(blue);
            hasil[4] = 1;
            if (hasil[1] > 0 && hasil[1] <= 57.7) {
                r1 = 1;
                r2 = 1;
                statr1 = 1;
                statr2 = 1;
            } else if (hasil[1] > 57.7 && hasil[1] <= 115.3) {
                r1 = (127.5 - hasil[1]) / (127.5 - 42.5);
                r2 = (hasil[1] - 42.5) / (127.5 - 42.5);
                statr1 = 1;
                statr2 = 2;
            } else if (hasil[1] > 127.5 && hasil[1] <= 212.5) {
                r1 = (212.5 - hasil[1]) / (212.5 - 127.5);
                r2 = (hasil[1] - 127.5) / (212.5 - 127.5);
                statr1 = 2;
                statr2 = 3;
            } else if (hasil[1] > 212.5) {
                r1 = 1;
                r2 = 1;
                statr1 = 3;
                statr2 = 3;
            }

            if (hasil[2] > 0 && hasil[2] <= 42.5) {
                g1 = 1;
                g2 = 1;
                statg1 = 1;
                statg2 = 1;
            } else if (hasil[2] > 42.5 && hasil[2] <= 127.5) {
                g1 = (127.5 - hasil[2]) / (127.5 - 42.5);
                g2 = (hasil[2] - 42.5) / (127.5 - 42.5);
                statg1 = 1;
                statg2 = 2;
            } else if (hasil[2] > 127.5 && hasil[2] <= 212.5) {
                g1 = (212.5 - hasil[2]) / (212.5 - 127.5);
                g2 = (hasil[2] - 127.5) / (212.5 - 127.5);
                statg1 = 2;
                statg2 = 3;
            } else if (hasil[2] > 212.5) {
                g1 = 1;
                g2 = 1;
                statg1 = 3;
                statg2 = 3;
            }


            if (hasil[3] > 0 && hasil[3] <= 42.5) {
                b1 = 1;
                b2 = 1;
                statb1 = 1;
                statb2 = 1;
            } else if (hasil[3] > 42.5 && hasil[3] <= 127.5) {
                b1 = (127.5 - hasil[3]) / (127.5 - 42.5);
                b2 = (hasil[3] - 42.5) / (127.5 - 42.5);
                statb1 = 1;
                statb2 = 2;
            } else if (hasil[3] > 127.5 && hasil[3] <= 212.5) {
                b1 = (212.5 - hasil[3]) / (212.5 - 127.5);
                b2 = (hasil[3] - 127.5) / (212.5 - 127.5);
                statb1 = 2;
                statb2 = 3;
            } else if (hasil[3] > 212.5) {
                b1 = 1;
                b2 = 1;
                statb1 = 3;
                statb2 = 3;
            }
            //r1
            if (statr1 == 1) {
                if (statg1 == 1) {
                    if (statb1 == 1) {
                        z1 = 20;
                    } else if (statb1 == 2) {
                        z1 = 20;
                    } else if (statb1 == 3) {
                        z1 = 20;
                    }
                } else if (statg1 == 2) {
                    if (statb1 == 1) {
                        z1 = 20;
                    } else if (statb1 == 2) {
                        z1 = 20;
                    } else if (statb1 == 3) {
                        z1 = 20;
                    }
                } else if (statg1 == 3) {
                    if (statb1 == 1) {
                        z1 = 20;
                    } else if (statb1 == 2) {
                        z1 = 20;
                    } else if (statb1 == 3) {
                        z1 = 20;
                    }
                }
            } else if (statr1 == 2) {
                if (statg1 == 1) {
                    if (statb1 == 1) {
                        z1 = 60;
                    } else if (statb1 == 2) {
                        z1 = 60;
                    } else if (statb1 == 3) {
                        z1 = 60;
                    }
                } else if (statg1 == 2) {
                    if (statb1 == 1) {
                        z1 = 60;
                    } else if (statb1 == 2) {
                        z1 = 60;
                    } else if (statb1 == 3) {
                        z1 = 60;
                    }
                } else if (statg1 == 3) {
                    if (statb1 == 1) {
                        z1 = 80;
                    } else if (statb1 == 2) {
                        z1 = 80;
                    } else if (statb1 == 3) {
                        z1 = 80;
                    }
                }
            } else if (statr1 == 3) {
                if (statg1 == 1) {
                    if (statb1 == 1) {
                        z1 = 100;
                    } else if (statb1 == 2) {
                        z1 = 100;
                    } else if (statb1 == 3) {
                        z1 = 100;
                    }
                } else if (statg1 == 2) {
                    if (statb1 == 1) {
                        z1 = 100;
                    } else if (statb1 == 2) {
                        z1 = 100;
                    } else if (statb1 == 3) {
                        z1 = 100;
                    }
                } else if (statg1 == 3) {
                    if (statb1 == 1) {
                        z1 = 100;
                    } else if (statb1 == 2) {
                        z1 = 100;
                    } else if (statb1 == 3) {
                        z1 = 100;
                    }
                }
            }
            //r2
            if (statr2 == 1) {
                if (statg2 == 1) {
                    if (statb2 == 1) {
                        z2 = 20;
                    } else if (statb2 == 2) {
                        z2 = 20;
                    } else if (statb2 == 3) {
                        z2 = 20;
                    }
                } else if (statg2 == 2) {
                    if (statb2 == 1) {
                        z2 = 20;
                    } else if (statb2 == 2) {
                        z2 = 20;
                    } else if (statb2 == 3) {
                        z2 = 20;
                    }
                } else if (statg2 == 3) {
                    if (statb2 == 1) {
                        z2 = 20;
                    } else if (statb2 == 2) {
                        z2 = 20;
                    } else if (statb2 == 3) {
                        z2 = 20;
                    }
                }
            } else if (statr2 == 2) {
                if (statg2 == 1) {
                    if (statb2 == 1) {
                        z2 = 60;
                    } else if (statb2 == 2) {
                        z2 = 60;
                    } else if (statb2 == 3) {
                        z2 = 60;
                    }
                } else if (statg2 == 2) {
                    if (statb2 == 1) {
                        z2 = 60;
                    } else if (statb2 == 2) {
                        z2 = 60;
                    } else if (statb2 == 3) {
                        z2 = 60;
                    }
                } else if (statg2 == 3) {
                    if (statb2 == 1) {
                        z2 = 80;
                    } else if (statb2 == 2) {
                        z2 = 80;
                    } else if (statb2 == 3) {
                        z2 = 80;
                    }
                }
            } else if (statr2 == 3) {
                if (statg2 == 1) {
                    if (statb2 == 1) {
                        z2 = 100;
                    } else if (statb2 == 2) {
                        z2 = 100;
                    } else if (statb2 == 3) {
                        z2 = 100;
                    }
                } else if (statg2 == 2) {
                    if (statb2 == 1) {
                        z2 = 100;
                    } else if (statb2 == 2) {
                        z2 = 100;
                    } else if (statb2 == 3) {
                        z2 = 100;
                    }
                } else if (statg2 == 3) {
                    if (statb2 == 1) {
                        z2 = 100;
                    } else if (statb2 == 2) {
                        z2 = 100;
                    } else if (statb2 == 3) {
                        z2 = 100;
                    }
                }
            }
            double[] apr1 = {r1, b1, g1};
            double[] apr2 = {r2, b2, g2};
            double w = (getMinValue(apr1) * z1) + (getMinValue(apr2) * z2) / (getMinValue(apr1) + getMinValue(apr2));
            Log.wtf("h", "" + w);
            Log.wtf("apr1", "" + getMinValue(apr1));
            Log.wtf("apr2", "" + getMinValue(apr2));
            if (w > 0 && w <= 20) {
                hasil[5] = 2;
            } else if (w > 20 && w <= 60) {
                hasil[5] = 3;
            } else if (w > 60 && w <= 80) {
                hasil[5] = 4;
            } else if (w > 80 && w <= 100) {
                hasil[5] = 5;
            } else {
                hasil[5] = 0;
            }
            hasil[6] = getMinValueint(srgb);
            return hasil;
        }
        // 2^5 for RGB 565

        public  int intensityLevel(final int color) {
            // also see Color#getRed(), #getBlue() and #getGreen()
            final int red = (color >> 16) & 0xFF;
            final int blue = (color >> 0) & 0xFF;
            final int green = (color >> 8) & 0xFF;
            assert red == blue && green == blue; // doesn't hold for green in RGB 565
            return MAX_INTENSITY - blue;
        }
        @Override
        protected void onPostExecute(int[] result) {
            if (result[5] == 2) {
                tv_1.setText("5 t/ha : 0 kg/ha , 6 t/ha : 0 atau 50 kg/ha , 7 t/ha : 50 kg/ha , 8 t/ha : 50 kg/ha");
            }
            if (result[5] == 3) {
                tv_1.setText("5 t/ha : 50 kg/ha , 6 t/ha : 75 kg/ha , 7 t/ha : 100 kg/ha , 8 t/ha : 125 kg/ha");
            }
            if (result[5] == 4) {
                tv_1.setText("5 t/ha : 50 kg/ha , 6 t/ha : 75 kg/ha , 7 t/ha : 100 kg/ha , 8 t/ha : 125 kg/ha");
            }
            if (result[5] == 5) {
                tv_1.setText("5 t/ha : 75 kg/ha , 6 t/ha : 100 kg/ha , 7 t/ha : 125 kg/ha , 8 t/ha : 150 kg/ha");

            }
            Log.wtf("h", "" + result[5]);
            tv_2.setText("Hasil modus S-RGB : " + result[0] + " ,modus R : " + result[1] + " modus G : " + result[2] + " modus B : " + result[3] + ", Nilai terkecil : " + result[6]);
            mCubicValueLineChart.addSeries(series);
            mCubicValueLineChart.startAnimation();
            mCubicValueLineChart2.addSeries(series2);
            mCubicValueLineChart2.startAnimation();
            mCubicValueLineChart3.addSeries(series3);
            mCubicValueLineChart3.startAnimation();
            mCubicValueLineChart4.addSeries(series4);
            mCubicValueLineChart4.startAnimation();
            mImageViewBinary.setImageDrawable(getDrawable(R.drawable.bwd));
            dialog.dismiss();
            if (result[4] == 0)
                TastyToast.makeText(getApplicationContext(), "Terjadi kesalahan", TastyToast.LENGTH_SHORT, TastyToast.ERROR);
            else
                TastyToast.makeText(getApplicationContext(), "Scaning berhasil", TastyToast.LENGTH_SHORT, TastyToast.SUCCESS);
        }
    }

    public static int[] convertIntegers(List<Integer> integers) {
        int[] ret = new int[integers.size()];
        for (int i = 0; i < ret.length; i++) {
            ret[i] = integers.get(i).intValue();
        }
        return ret;
    }

    // getting the maximum value
    public static int getMaxValue(int j, int[] array) {
        int maxValue = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == j) {
                maxValue++;
            }
        }
        return maxValue;
    }


    // getting the miniumum value
    public static double getMinValue(double[] array) {
        double minValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < minValue) {
                minValue = array[i];
            }
        }
        return minValue;
    }

    public static int getMinValueint(int[] array) {
        int minValue = array[0];
        for (int i = 1; i < array.length; i++) {
            if (array[i] < minValue) {
                minValue = array[i];
            }
        }
        return minValue;
    }

}